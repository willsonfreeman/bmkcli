# bmkcli - url bookmark cli tool
# See LICENSE file for copyright and license details.

include config.mk

SRC = bmkcli.c parsedb.c util.c
OBJ = ${SRC:.c=.o}
XDG_DATA_DIR ?= ~/.local/share

all: bmkcli options bmkcli.1.gz bookmark.db

options:
	@echo "bmkcli build options"
	@echo "   CFLAGS = $(CFLAGS)"
	@echo "   CC     = $(CC)"

bmkcli: ${OBJ}
	@${CC} -o $@ ${OBJ} ${CFLAGS}

${OBJ}: config.h

config.h: config.def.h
	@cp config.def.h $@

%.o: %.c %.h
	@${CC} -c ${CFLAGS} $<

clean:
	@rm -f bmkcli ${OBJ} bmkcli.1.gz

bmkcli.1.gz: bmkcli.1
	@sed "s/VERSION/${VERSION}/g" < bmkcli.1 | gzip -k --best -c > $@

install: all
	@mkdir -p ${PREFIX}/bin
	@install -Dm755 bmkcli ${PREFIX}/bin/bmkcli
	@mkdir -p ${MANPREFIX}/man1
	@install -Dm644 bmkcli.1.gz ${MANPREFIX}/man1/bmkcli.1.gz
	@mkdir -p ${PREFIX}/share/licenses/bmkcli
	@install -Dm644 LICENSE ${PREFIX}/share/licenses/bmkcli/LICENSE

bookmark.db: bookmarks-example.db
	@mkdir -p $(XDG_DATA_DIR)/bmkcli
	@cp -n bookmarks-example.db $(XDG_DATA_DIR)/bmkcli/bookmarks.db
	@chmod 600 $(XDG_DATA_DIR)/bmkcli/bookmarks.db

uninstall:
	@rm -f ${PREFIX}/bin/bmkcli
	@rm -f ${MANPREFIX}/man1/bmkcli.1.gz
	@rm -f ${PREFIX}/share/licenses/bmkcli/LICENSE

.PHONY: clean all install uninstall
