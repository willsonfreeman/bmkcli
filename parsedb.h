typedef struct Entry Entry;
struct Entry {
	char name[1024];
	char category[1024];
	char taglist[1024];
	char url[1024];
	char hidden[1024];
	Entry *next;
};

void allocateentry(Entry **e);
void addproperty(char *dest, char *src);
void freeentrieslist(Entry *e);
char *getfieldvalue(char *string);
char *getfieldname(char *string);
char *removesurroundwhitespace(char *string);
void parsedb(Entry **e, FILE *db);
