# bmkcli

cli url bookmark utility

goal list
---

a short list of goals for bmkcli

* [x] list bookmarks
* [x] get bookmark url
* [ ] save new bookmark
* [ ] edit existing bookmark
* [x] pre-filter results based on tag/category

build instructions
---

to install simply run the following as your user.

```
$ make all
$ sudo make install
```

**NOTE**:

the **[all](Makefile#L42-45)** target in the [Makefile](Makefile) will copy the example [bookmarks](bookmarks-example.db) file to either **XDG\_DATA\_DIR/bmkcli/bookmarks.db** or **~/.local/share/bmkcli/bookmarks.db** if **XDG\_DATA\_DIR** is unset.


to uninstall from system simply run the following as your user.

```
$ sudo make uninstall
```

the bookmark.db file isn't automagically deleted in case you want or need to extract from it, but can be deleted by running the following as your user.

```
$ rm -I -rf ${XDG_DATA_DIR:-~/.local/share}/bmkcli
```

examples
---

also found in manual page bmkcli.1

```
$ man bmkcli
```

to get a list of all non-hidden bookmarks in the bookmarks database to stdout.

```
$ bmkcli -l
```

to get a list of all bookmarks in the bookmark database to stdout.

```
$ bmkcli -l -I
```

to list out all non-hidden bookmarks in the bookmark database and display it out in dmenu for user selection, selection is sent to stdout.

```
$ bmkcli -l | dmenu -l 5 -p '>'
```

to first list out all non-hidden bookmarks in the bookmark database and  display it out in *dmenu* for user selection, and then print the *url* of the selected bookmark to stdout.

```
$ bmkcli -g "$(bmkcli -l | dmenu -l 5 -p '>')"
```

to first list out all non-hidden bookmarks in the bookmark database and  display  it out in *dmenu* for user selection, and then copy the *url* of the selected bookmark into the system clipboard.

```
$ bmkcli -g "$(bmkcli -l | dmenu -p 'Select:' -l 5)" | xclip -r -selection clipboard
```

to find all non-hidden bookmarks with either a category that matches "blogpost" or one that has "blogpost" in its tags.

```
$ bmkcli -s "blogpost"
```

files
---

**bookmarks.db**

bookmark entries are kept in this format:

```
[bookmark]
Name:       bmkcli gitlab page
Url:        https://gitlab.com/willsonfreeman/bmkcli
Category:   software
Tags:       software,code,c,makefiles,groff,shell,posix
Hidden:     No
```

note that the indenting is not required. there can in-fact be no spaces between the **:** and the field values, like so:

```
[bookmark]
Name:bmkcli gitlab page
Url:https://gitlab.com/willsonfreeman/bmkcli
Category:software
Tags:software,code,c,makefiles,groff,shell,posix
Hidden:No
```

the fields can be in different orders as well, like so:

```
[bookmark]
Category:software
Hidden:No
Url:https://gitlab.com/willsonfreeman/bmkcli
Tags:software,code,c,makefiles,groff,shell,posix
Name:bmkcli gitlab page
```

the **Hidden** field can be omitted, it defaults to **No** in this case.

all other fields can technically be omitted, they will default to **n/a** in such cases.

# license

see LICENSE file.
