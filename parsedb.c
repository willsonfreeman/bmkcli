#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

#include "util.h"
#include "parsedb.h"

void
parsedb(Entry **e, FILE *db)
{
	char *line = NULL;
	size_t linesize = 0;
	ssize_t linelen;

	if (*e)
		die("entries list not null before parse");

	while ((linelen = getline(&line, &linesize, db)) != -1)
		if (!strcmp(line, "[bookmark]\n"))
			allocateentry(&(*e));
		else if (!strcmp(line, "\n") || removesurroundwhitespace(line)[0] == '#')
			continue;
		else
			if (strcmp(line, "\n")) {
				char *p = removesurroundwhitespace(getfieldname(line));
				if (!strcmp(p, "Name"))
					addproperty((*e)->name, removesurroundwhitespace(getfieldvalue(line)));
				else if (!strcmp(p, "Url"))
					addproperty((*e)->url, removesurroundwhitespace(getfieldvalue(line)));
				else if (!strcmp(p, "Category"))
					addproperty((*e)->category, removesurroundwhitespace(getfieldvalue(line)));
				else if (!strcmp(p, "Tags"))
					addproperty((*e)->taglist, removesurroundwhitespace(getfieldvalue(line)));
				else if (!strcmp(p, "Hidden"))
					addproperty((*e)->hidden, removesurroundwhitespace(getfieldvalue(line)));
				free(p);
			}

	free(line);

	if (!(*e))
		die("entries");
}

void
addproperty(char *dest, char *src)
{
	if (!dest)
		die("cannot addproperty, is null");

	if (strlen(src) > 0)
		memcpy(dest, src, strlen(src));
}

void
allocateentry(Entry **e)
{
	Entry *tmp = NULL;
	tmp = *e;
	*e = ecalloc(1, sizeof(Entry));
	(*e)->next = tmp;
}

void
freeentrieslist(Entry *e)
{
	Entry *tmp = NULL;
	while (e != NULL) {
		tmp = e;
		e = e->next;
		free(tmp);
	}
}

char *
getfieldvalue(char *string)
{
	static char *p = NULL;
	if (!(p = strchr(string, ':')))
		return NULL;

	return (p + 1);
}

char *
getfieldname(char *string)
{
	static char *p = NULL, *str = NULL;
	if (!(p = strchr(string, ':')))
		return NULL;

	ptrdiff_t i;
	i = p - string;
	str = calloc(i, sizeof(char));
	memcpy(str, string, i);

	return str;
}

char *
removesurroundwhitespace(char *string)
{
	static char *str = NULL;
	int count = 0;

	while (string[count] == ' ' || string[count] == '\t')
		count++;

	str = string + count;
	count = strlen(str);

	while (str[count - 1] == ' ' || str[count - 1] == '\t' || str[count - 1] == '\n')
		count--;

	str[count] = '\0';
	return str;
}
