/*
 * Copyright © 2020 willson freeman - MIT/X Consortium License
 * see LICENSE file for more info
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"
#include "parsedb.h"

/* macros */
#define BUFFSIZE 80

/* initialized values for cli flags */
static unsigned int listentries = 0;
static unsigned int getentryurl = 0;
static unsigned int searchentry  = 0;
#include "config.h"

/* initialized arguments for cli flags */
static const char *entrystring = NULL;
static const char *searchstring = NULL;

static char *findbookmarkdb(void);
static void getbookmarkurl(Entry *e, const char *match);
static void listbookmarks(Entry *e);
static void searchbystring(Entry *e, const char *match);
static int  matchwithtags(Entry *e, const char *match);
static void usage(void);

static char *
findbookmarkdb(void)
{
	char *home = NULL, *path = NULL;
	int n = 0;

	path = reallocarray(path, sizeof(char), BUFFSIZE);
	if (!(home = getenv("XDG_DATA_HOME")))
		die("getenv");

	n = snprintf(path, BUFFSIZE, "%s%s", home, "/bmkcli/bookmarks.db");
	if (n >= BUFFSIZE) {
		path = reallocarray(path, sizeof(char), n);
		snprintf(path, n + 1, "%s%s", home, "/bmkcli/bookmarks.db");
	} else if (n == -1)
		die("snprintf");

	return path;
}

static void
getbookmarkurl(Entry *e, const char *match)
{
	if (!e)
		die("no bookmarks found");

	Entry *ret = NULL;
	for (; e && !(ret); e = e->next)
		if ((strstr(match, e->category)) && (strstr(match, e->name)))
			ret = e;

	if (ret)
		fprintf(stdout, "%s\n", ret->url);
	else
		die("no match found");
}

static void
listbookmarks(Entry *e)
{
	if (!e)
		die("no bookmarks found");

	for (; e; e = e->next)
		if (showhidden != 1 && !strcasecmp(e->hidden, "yes"))
			continue;
		else if (!strcmp(e->url, ""))
			continue;
		else
			fprintf(stdout, "%s > %s\n", e->category, e->name);
}

static void
searchbystring(Entry *e, const char *match)
{
	if (!e)
		die("no bookmarks found");

	for (; e; e = e->next)
		if (showhidden != 1 && !strcasecmp(e->hidden, "yes"))
			continue;
		else
			if (!strcmp(e->category, match))
				fprintf(stdout, "%s > %s\n", e->category, e->name);
			else if (!strcmp(e->taglist, ""))
				continue;
			else if (matchwithtags(e, match) == 1)
				fprintf(stdout, "%s > %s\n", e->category, e->name);
}

static int
matchwithtags(Entry *e, const char *match)
{
	int i = -1, len = strlen(e->taglist);
	char *temp = NULL;

	temp = ecalloc(sizeof(char), len - 1);
	memcpy(temp, e->taglist, len);
	while (temp[++i]) {
		if (temp[i] == ',') {
			temp[i] = '\0';
			if (!strcmp(temp, match))
				return(1);
			else {
				temp[i] = ',';
				temp = temp + i + 1;
				i = -1;
			}
		}
	}
	return(-1);
}

static void
usage(void)
{
	fputs("usage: bmkcli [-hlI] [-g <string>] [-s <string>]\n", stderr);
	exit(1);
}

int
main(int argc, char **argv)
{
	for (int i = 1; i < argc; i++)
		if (!strcmp(argv[i], "-l"))
			listentries = 1;
		else if (!strcmp(argv[i], "-I"))
			showhidden = 1;
		else if (!strcmp(argv[i], "-h"))
			usage();
		else if (i + 1 == argc)
			usage();
		else if (!strcmp(argv[i], "-g")) {
			getentryurl = 1;
			entrystring = argv[++i];
		} else if (!strcmp(argv[i], "-s")) {
			searchentry = 1;
			searchstring = argv[++i];
		} else
			usage();

	if (listentries + getentryurl + searchentry != 1)
		usage();

	char *path = NULL;
	path = findbookmarkdb();
	FILE *db = fopen(path, "r");
	free(path);

	if (!db)
		exit(1);

	Entry *entries = NULL;
	parsedb(&entries, db);
	fclose(db);

	if (listentries)
		listbookmarks(entries);
	else if (getentryurl)
		getbookmarkurl(entries, entrystring);
	else if (searchentry)
		searchbystring(entries, searchstring);

	freeentrieslist(entries);
	return(0);
}
